
package rekursiv.funktion;


public class RekursivFunktion {

    
    //Anropar metoden rekursion och sätter int variablerna till 10.
    //metoden anropas så länge rek och i är över 0.
    //Därefter printas "Klart" ut
    public static void rekursion(int rek, int i){
            System.out.println("Recurse "+i);
        
        if(rek<0){
            System.out.println("Klart");
            
        }
        else
            
            rekursion(rek-1, i-1);
            i++;
    }
    
    public static void main(String[] args) {
        
        rekursion(10,10);
    }
    
}
